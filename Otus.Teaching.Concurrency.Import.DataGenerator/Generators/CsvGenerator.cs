﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class CsvGenerator : IDataGenerator
    {
        private readonly string _fileName;
        private readonly int _dataCount;

        public CsvGenerator(string fileName, int dataCount)
        {
            _fileName = fileName;
            _dataCount = dataCount;
        }

        public void Generate()
        {
            var customers = RandomCustomerGenerator.Generate(_dataCount);
            var serializer = new CsvSerializer();

            using var fs = new FileStream(_fileName, FileMode.Create, FileAccess.Write);
            using StreamWriter writer = new StreamWriter(fs);
            foreach (var customer in customers)
            {
                string json = serializer.Serialize<Customer>(customer);
                writer.WriteLine(json);
            }
        }
    }
}
