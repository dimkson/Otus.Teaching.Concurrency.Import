using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Concurrency.Import.Handler.Repositories
{
    public interface ICustomerRepository
    {
        void AddCustomers(List<Customer> customers);
        void AddCustomer(Customer customer);
        void Save();
        IEnumerable<Customer> GetCustomers();
        Task<Customer> GetCustomerAsync(int id);
    }
}