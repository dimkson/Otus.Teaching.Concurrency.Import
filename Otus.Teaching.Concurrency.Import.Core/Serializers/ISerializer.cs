﻿namespace Otus.Teaching.Concurrency.Import.Core.Serializers
{
    public interface ISerializer
    {
        string Serialize<T>(T obj);
        T Deserialize<T>(string text);
    }
}
