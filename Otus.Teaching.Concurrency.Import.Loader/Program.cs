﻿using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customers.json");
        private static int _dataCount;

        static void Main(string[] args)
        {
            Console.WriteLine($"Loader started with process Id {Process.GetCurrentProcess().Id}...");

            if (TryValidateAndParseArgs(args))
            {
                Process.Start(_dataFilePath, $"customers{ (args.Length > 1 ? " " + _dataCount.ToString() : "") }").WaitForExit();
                _dataFilePath = Path.Combine(Path.GetDirectoryName(_dataFilePath), $"customers.json");
            }
            else
            {
                GenerateCustomersDataFile();
            }

            var parser = new CsvParser(_dataFilePath);
            var list = parser.Parse();

            var loader = new ThreadPoolDataLoader(list, 4);
            loader.LoadData();

            Console.WriteLine("Data uploaded");
        }

        static void GenerateCustomersDataFile()
        {
            //var generator = new XmlGenerator(_dataFilePath, 1_000_000);
            var generator = new CsvGenerator(_dataFilePath, 1_000_000);
            generator.Generate();
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            if (args != null && args.Length > 0)
            {
                if (args.Length > 1)
                {
                    if (!int.TryParse(args[1], out _dataCount))
                    {
                        Console.WriteLine("Data must be integer");
                        return false;
                    }
                }
                _dataFilePath = args[0];
            }
            else
            {
                return false;
            }

            return true;
        }
    }
}