﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    internal class MultiThreadDataLoader : IDataLoader
    {
        private readonly List<Customer> _list;
        private readonly int _threadCount;
        private readonly ContextFactory _factory;

        public MultiThreadDataLoader(List<Customer> list, int threadCount)
        {
            _list = list;
            _threadCount = threadCount;
            _factory = new ContextFactory();
        }

        public void LoadData()
        {
            var sw = new Stopwatch();
            sw.Start();
            var threads = new List<Thread>();
            
            for (int i = 0; i < _threadCount; i++)
            {
                threads.Add(new Thread(InserToDb));
                threads[i].Start(i);
            }
            threads.ForEach(t => t.Join());
            sw.Stop();
            Console.WriteLine($"Время затраченное на обработку данных: {sw.Elapsed}{Environment.NewLine} Кол-во потоков: {_threadCount}");
        }

        private void InserToDb(object obj)
        {
            int position = (int)obj;
            int step = _list.Count / _threadCount;
            int startPosition = step * position;
            CustomerRepository repository = new CustomerRepository(_factory.CreateDbContext());
            int tries = 3;

            while (tries > 0)
            {
                try
                {
                    repository.AddCustomers(_list.GetRange(startPosition, step));
                    repository.Save();

                    // Учет элементов, оставшихся в случае невозможности целочисленного деления коллекции на кол-во потоков
                    if (position == _threadCount - 1)
                    {
                        int ost = _list.Count % step;
                        if (ost != 0)
                        {
                            repository.AddCustomers(_list.GetRange(_list.Count - ost, ost));
                            repository.Save();
                        }
                    }
                    tries = 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Повторная попытка записи в базу, осталось попыток {tries}");
                    Console.WriteLine($"{ex.Message}\n {ex.InnerException.Message}");
                    tries--;
                }
            }
        }
    }
}
