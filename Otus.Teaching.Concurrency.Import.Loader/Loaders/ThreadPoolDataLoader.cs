﻿using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    internal class ThreadPoolDataLoader : IDataLoader
    {
        private readonly List<Customer> _list;
        private readonly int _threadCount;
        private readonly ContextFactory _factory;
        private readonly ManualResetEvent[] _resetEvents;

        public ThreadPoolDataLoader(List<Customer> list, int threadCount)
        {
            _list = list;
            _threadCount = threadCount;
            _factory = new ContextFactory();
            _resetEvents = new ManualResetEvent[_threadCount];
        }

        public void LoadData()
        {
            var sw = new Stopwatch();
            sw.Start();

            for (int i = 0; i < _threadCount; i++)
            {
                _resetEvents[i] = new ManualResetEvent(false);
                ThreadPool.QueueUserWorkItem(InserToDb, i);
            }

            WaitHandle.WaitAll(_resetEvents);
            sw.Stop();
            Console.WriteLine($"Время затраченное на обработку данных: {sw.Elapsed}{Environment.NewLine}Кол-во потоков: {_threadCount}");
        }

        private void InserToDb(object obj)
        {
            int position = (int)obj;
            int step = _list.Count / _threadCount;
            int startPosition = step * position;
            CustomerRepository repository = new CustomerRepository(_factory.CreateDbContext());
            int tries = 3;

            while (tries > 0)
            {
                try
                {
                    repository.AddCustomers(_list.GetRange(startPosition, step));
                    repository.Save();
                    tries = 0;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Повторная попытка записи в базу, осталось попыток {tries}");
                    Console.WriteLine($"{ex.Message}{Environment.NewLine}{ex.InnerException.Message}");
                    tries--;
                }
            }
            _resetEvents[position].Set();
        }
    }
}
