﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Serializers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser
    {
        private readonly string _path;

        public CsvParser(string path)
        {
            _path = path;
        }

        public List<Customer> Parse()
        {
            //Parse data
            CsvSerializer serializer = new CsvSerializer();

            using FileStream fs = new FileStream(_path, FileMode.Open, FileAccess.Read);
            using StreamReader reader = new StreamReader(fs);

            var list = new List<Customer>();
            while (!reader.EndOfStream)
            {
                list.Add(serializer.Deserialize<Customer>(reader.ReadLine()));
            }

            return list;
        }
    }
}
