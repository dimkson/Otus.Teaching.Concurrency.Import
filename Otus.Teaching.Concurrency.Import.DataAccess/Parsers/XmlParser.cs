﻿using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Xml.Serialization;
using System.IO;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser
        : IDataParser<List<Customer>>
    {
        private readonly string _path;

        public XmlParser(string path)
        {
            _path = path;
        }

        public List<Customer> Parse()
        {
            //Parse data
            StreamReader streamReader = new StreamReader(_path);

            XmlSerializer serializer = new XmlSerializer(typeof(CustomersList));

            var list = (CustomersList)serializer.Deserialize(streamReader);
            return list.Customers;
        }
    }
}