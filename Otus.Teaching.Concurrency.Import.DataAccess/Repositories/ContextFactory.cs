﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class ContextFactory : IDbContextFactory<CustomersContext>
    {
        public CustomersContext CreateDbContext()
        {
            DbContextOptionsBuilder<CustomersContext> optionsBuilder = new DbContextOptionsBuilder<CustomersContext>();

            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json");
            IConfiguration config = builder.Build();

            string connectionString = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseNpgsql(connectionString);
            return new CustomersContext(optionsBuilder.Options);
        }
    }
}
