﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;

namespace ConsoleClient
{
    class Program
    {
        private static HttpClient _client;

        static void Main(string[] args)
        {
            Console.WriteLine("Введите Id покупателя");
            int id;
            while (!int.TryParse(Console.ReadLine(), out id))
            {
                Console.WriteLine("Необходимо ввести число. Попробуйте еще раз!");
            }

            Init();
            var customer = GetAsync($"users/{id}").Result;

            if (customer != null)
                Console.WriteLine($"{customer}");
            else
                Console.WriteLine($"Пользователь c id-{id} не найден!");

            Console.WriteLine("Генерация случайного пользователя...");
            id = new Random().Next(1, 1_000_000);
            customer = RandomCustomerGenerator.GenerateOne(id);
            var newCustomer = PostAsync("users/PostCustomer", customer).Result;
            if (newCustomer != null)
                Console.WriteLine($"Пользователь {newCustomer} добавлен в базу");
            else
                Console.WriteLine($"Пользователь c id-{id} уже существует!");

            Console.ReadKey();
        }

        private static void Init()
        {
            _client = new HttpClient();
            _client.BaseAddress = new Uri("https://localhost:44329/");
            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        }

        public static async Task<Customer> GetAsync(string path)
        {
            HttpResponseMessage response = await _client.GetAsync(path);
            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<Customer>()
                : null;
        }

        public static async Task<Customer> PostAsync(string path, Customer customer)
        {
            var response = await _client.PostAsJsonAsync(path, customer);
            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<Customer>()
                : null;
        }
    }
}
